package net.maku.storage.enums;

/**
 * 存储类型枚举
 *
 * @author 阿沐 babamu@126.com
 */
public enum StorageTypeEnum {
    /**
     * 本地
     */
    LOCAL,
    /**
     * 阿里云
     */
    ALIYUN;
}
